import React, {Component} from 'react';

/**
 * Composant graphique affichant la page 404 - not found
 *
 * @class NoPage
 * @extends {Component}
 */
class NoPage extends Component {

    render(){
        return (
            <div className="noPage">
                <div className="cont_principal cont_error_active">
                    <div className="cont_error">
                        <h1>Erreur</h1>  
                        <p>Cette page n'existe pas.</p>
                    </div>
                    <div className="cont_aura_1"></div>
                    <div className="cont_aura_2"></div>
                </div>
            </div>
        );
    }
}
export default NoPage;