import React, {Fragment} from 'react';

import { format } from 'date-fns';
import { fr } from 'date-fns/locale';

/**
 * Composant qui converti les dates retournées par l'api pour affichage
 *
 * @param {date} props
 * @return {Component} 
 */
function DateFormat(props) {
    let date = new Date(props.date);
    let formatDate = format(date,'dd MMMM y à H:mm',{locale:fr});
    return <Fragment>{formatDate}</Fragment>;
  }

export default DateFormat