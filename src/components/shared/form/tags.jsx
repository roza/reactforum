import React, { Component } from 'react';

/**
 * Composant Input des tags pour les posts, predictions et multi inputs
 * Enfant de UpdatePost et NewPostForm
 * Props requis: tagsList (fonction parente pour retourner les tags), tags (array tags du post en cas de modif),
 * suggestions (array tags existants du forum) 
 *
 * @class Tags
 * @extends {Component}
 */
class Tags extends Component {
    /**
     * Creates an instance of Tags.
     * @param {tagsList, tags, suggestions} props
     * @memberof Tags
     */
    constructor(props) {
      super(props);
      
      this.state = {
        tags: this.props.tags,
        suggestList: [],
        activeSuggestion: 0,
        filteredSuggestions: [],
        showSuggestions: false,
        userInput: ''
      };
    }

    /**
     * DidMount recupere les props de suggestions et en fait un tableau suggestList 
     *
     * @memberof Tags
     */
    componentDidMount() {
      if(this.props.suggestions){
        let suggestList = [];
        this.props.suggestions.forEach(element => {
          suggestList.push(element.title)
        });
        this.setState({suggestList: suggestList})
      }      
    }

    /**
     * Fonction pour delete un tag onclick sur la croix du tag et focus auto
     *
     * @param {int} i index du tableau de tags
     * @memberof Tags
     */
    removeTag = (i) => {
      const input = document.querySelector("#tagInput");
      input.focus();
      const newTags = [ ...this.state.tags ];
      newTags.splice(i, 1);
      this.props.tagsList(newTags);
      this.setState({ tags: newTags });
      
    }
  
    /**
     * Gestion des touches enter et backspace pour ajouter et supprimer un tag
     *
     * @param {event} e
     * @memberof Tags
     */
    inputKeyDown = (e) => {
      const val = e.target.value;
      if (e.key === 'Enter' && val) {
        if (this.state.tags.find(tag => tag.toLowerCase() === val.toLowerCase())) {
          return;
        }
        this.props.tagsList([...this.state.tags, val]);
        this.setState({ tags: [...this.state.tags, val], userInput: ''});
        
        //this.tagInput.value = null;
      } else if (e.key === 'Backspace' && !val) {
        this.removeTag(this.state.tags.length - 1);
      }
    }
  
    //SUGGESTIONS
    /**
     * Permet de lister les tags existants en fonction du contenu de l'input
     *
     * @param {event} e
     * @memberof Tags
     */
    onChange = e => {
      const suggestList = this.state.suggestList;
      const userInput = e.currentTarget.value;
  
      const filteredSuggestions = suggestList.filter(
        suggestion =>
          suggestion.toLowerCase().indexOf(userInput.toLowerCase()) > -1
      );
  
      this.setState({
        activeSuggestion: 0,
        filteredSuggestions,
        showSuggestions: true,
        userInput: e.currentTarget.value
      });
    };

    /**
     * OnClick sur une suggestion ajoute un tag et remet le focus sur l'input
     *
     * @param {event} e
     * @memberof Tags
     */
    onClick = e => {
      const input = document.querySelector("#tagInput");
      input.focus();
      this.props.tagsList([...this.state.tags, e.currentTarget.innerText]);
      this.setState({
        activeSuggestion: 0,
        filteredSuggestions: [],
        showSuggestions: false,
        tags: [...this.state.tags, e.currentTarget.innerText],
        userInput: ''
      });
      
    };

    render() {
      // listes des tags entré par l'utilisateur
      const { tags } = this.state;
      //suggestion depuis liste des tags existants (appel a l'api fait dans le composant parent)
      let suggestionsListComponent;
      if (this.state.showSuggestions && this.state.userInput) {
        if (this.state.filteredSuggestions.length) {
          suggestionsListComponent = (
            <ul className="suggestions">
              <em style={{marginRight:"5px"}}>Suggestions: </em>
              {this.state.filteredSuggestions.map((suggestion, index) => {
                
                return (
                  <li  key={suggestion} onClick={this.onClick}>
                    {suggestion}
                  </li>
                );
              })}
            </ul>
          );
        } else {
          suggestionsListComponent = (
            <div className="no-suggestions">
              <em>Aucune suggestion!</em>
            </div>
          );
        }
      }

      return (
        <div className="input-tag">
          <ul className="input-tag__tags">
            { tags.map((tag, i) => (
              <li key={tag}>
                {tag}
                <button type="button" onClick={() => { this.removeTag(i); }}>+</button>
              </li>
            ))}
            <li className="input-tag__tags__input"><input id="tagInput" type="text" value={this.state.userInput} onChange={this.onChange} onKeyDown={this.inputKeyDown} ref={c => { this.tagInput = c; }} /></li>
            
          </ul>
          {suggestionsListComponent}
        </div>
      );
    }
  }
  
export default Tags;
  