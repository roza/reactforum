import React, {Component} from 'react';

/**
 * Composant permettant l'affichage de l'animation en background
 *
 * @class Background
 * @extends {Component}
 */
class Background extends Component {

    render(){
        return (
            <div className="noPage">
                <div className="cont_principal cont_error_active">
                    <div className="cont_aura_1"></div>
                    <div className="cont_aura_2"></div>
                </div>
            </div>
        );
    }
}
export default Background;