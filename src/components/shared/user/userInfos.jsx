import React, { Component, Fragment } from 'react';

import LoadingSpinner from '../utils/loadingSpinner';
import Tabs from 'react-bootstrap/Tabs';
import Tab from 'react-bootstrap/Tab';

import { API, APPNAME } from '../../constants.js';
import TableLine from '../post/TableLine';
import TableLineResponse from '../post/TableLineResponse';
import Pagination from '../utils/pagination';
import AdminPost from '../admin/adminPost';
import AdminCategories from '../admin/adminCategories';
import AdminUsers from '../admin/adminUsers';

/**
 * Composant affichant la grilles des articles d'un utilisateur
 * Affiche aussi la grilles des réponses d'un utilisateur
 * Affiche la page d'administration si user connecté est Admin du forum
 * Enfant de User, Parent de TableLine, TableLineResponse, Pagination, 
 * AdminPost, AdminCategories, AdminUsers
 * Props requis: id utilisateur et username(grace aux params url) infos user connecté, 
 * isAuth(boolean de connexion), fonction de reload des catégories pour la navbar
 *
 * @class UserInfos
 * @extends {Component}
 */
class UserInfos extends Component {
    
    state = {
        loading: true,
        posts: [],
        responses: [],
        nbPosts: 0,
        nbResponses: 0,
        currentPage : 1,
        currentPageResponse: 1,
    }

    /**
     * Appel à l'api pour récuperer les posts d'un utilisateur choisi
     *
     * @param {number} [newPage=1] Page à charger
     * @memberof UserInfos
     */
    getUserPosts(newPage = 1){
        const page = newPage;
        const filters = `?order[createdAt]=desc&author.id=${this.props.id}&page=${page}`;
        const requestOptions = {
            method: 'GET',
            headers: { 'Content-Type': 'application/json' },
            credentials: 'omit'
        };
        fetch(`${API}/posts`+filters, requestOptions)
        .then(res => res.json())
            .then(res => {
                if(res['hydra:totalItems'] !== 0){
                    let nbPages;
                    let nbPosts = res['hydra:totalItems'];
                    if(res['hydra:view'].hasOwnProperty(['hydra:last'])){
                    const iriLastPages = res['hydra:view']['hydra:last'];
                    const chars = iriLastPages.split('&page=');
                    nbPages = chars[1]
                    } else{
                        if(this.state.currentPage !== 1){
                        this.getPosts(1)
                        }else{
                        nbPages = "1";
                        }
                    }
                    this.setState({ 
                        nbPosts: nbPosts,
                        posts: res['hydra:member'],
                        nbPages: nbPages,
                        currentPage: page
                    })
                    window.scrollTo(0, 0)
                }else{
                    this.setState({ 
                        nbPosts: 0
                    })
                }
            })
    }
    
    /**
     * Appel à l'api pour récuperer les réponses d'un utilisateur choisi
     *
     * @param {number} [newPage=1] Page à charger
     * @memberof UserInfos
     */
    getUserResponses(newPage = 1){
        const pageResponse = newPage;
        const filters = `?order[createdAt]=desc&author=${this.props.id}&page=${pageResponse}`;
        const requestOptions = {
            method: 'GET',
            headers: { 'Content-Type': 'application/json' },
            credentials: 'omit'
        };
        fetch(`${API}/responses`+filters, requestOptions)
        .then(res => res.json())
            .then(res => {
                if(res['hydra:totalItems'] !== 0){
                    let nbPagesResponses;
                    let nbResponses = res['hydra:totalItems'];
                    if(res['hydra:view'].hasOwnProperty(['hydra:last'])){
                        const iriLastPages = res['hydra:view']['hydra:last'];
                        const chars = iriLastPages.split('&page=');
                        nbPagesResponses = chars[1]
                    } else{
                        nbPagesResponses = "1";
                    }
                    this.setState({ 
                        nbResponses: nbResponses,
                        responses: res['hydra:member'],
                        nbPagesResponses: nbPagesResponses,
                        currentPageResponse: pageResponse,
                        loading: false
                    })
                    window.scrollTo(0, 0)
                }else{
                    this.setState({ 
                        nbResponses: 0,
                        loading: false
                    })
                }
            })
    }

      
   /**
   * Fonction permettant un affichage du loader avec durée choisi
   *
   * @return {*} 
   * @memberof App
   */
    loadingTime(){
        return new Promise(resolve => setTimeout(resolve, 600))
    }

    /**
     * Affichage du loader puis appel à l'api pour les infos
     *
     * @memberof UserInfos
     */
    componentDidMount(){
        this.loadingTime().then(() => {
            this.getUserPosts();
            this.getUserResponses();
        })
    }

    /**
     * Fonction pour reload la liste de posts en cas d'update/delete d'un post
     *
     * @param {*} newPage Page actuelle
     * @memberof UserInfos
     */
    pageChange(newPage){
        this.getUserPosts(newPage);
    }

    /**
     * Fonction pour reload les données en cas d'update/delete d'une réponse
     *
     * @param {*} newPage Page actuelle
     * @memberof UserInfos
     */
    pageChangeResponse(newPage){
        this.getUserResponses(newPage);
    }

    render(){
        let defaultTab;
        if(this.props.userConnect.roles.includes("ROLE_ADMIN")){
            defaultTab = "admin";
        }else{
            defaultTab = "posts";
        }
        return (
            <Fragment>
                {this.state.loading ? 
                    <LoadingSpinner/>
                :
                <Fragment> 
                    <Tabs className="justify-content-center " defaultActiveKey={defaultTab} id="user-tab" transition={false}>
                        <Tab tabClassName="" eventKey="posts" title={`Les posts de ${this.props.username}`}>
                            <div className="container">
                                <div className="row">
                                    {this.state.nbPosts !== 0 ?
                                        this.state.posts.map((post,i , arr) => {
                                            return(
                                                (this.props.userConnect.username === this.props.username || this.props.userConnect.roles.includes("ROLE_ADMIN"))?
                                                    <TableLine key={post.id} postInfos={post} updateList={() => this.getUserPosts(this.state.currentPage)} updateDel={true}/>
                                                :
                                                    <TableLine key={post.id} postInfos={post} updateList={() => this.getUserPosts(this.state.currentPage)} updateDel={false}/>
                                            )                                   
                                        })
                                    :
                                        <p>Cet utilisateur n'a jamais posé de question</p>
                                    }
                                </div>
                                {this.state.nbPages > 1 && <Pagination maxPage={this.state.nbPages} currentPage={this.state.currentPage} pageChange={this.pageChange.bind(this)}/>}
                            </div>    
                        </Tab>
                        <Tab eventKey="responses" title={`Les réponses de ${this.props.username}`}>
                            <div className="container">
                                <div className="row">
                                    {this.state.nbResponses !== 0 ?
                                        this.state.responses.map((response,i , arr) => {
                                            return(
                                                (this.props.userConnect.username === this.props.username || this.props.userConnect.roles.includes("ROLE_ADMIN")) ?
                                                    <TableLineResponse key={response.id} responsesInfos={response} updateList={() => this.getUserResponses(this.state.currentPageResponse)} updateDel={true}/>
                                                :
                                                    <TableLineResponse key={response.id} responsesInfos={response} updateList={() => this.getUserResponses(this.state.currentPageResponse)} updateDel={false}/>
                                                )                                   
                                        })
                                    :
                                        <p>Cet utilisateur n'a jamais répondu a des question</p>
                                    }
                                </div>
                                {this.state.nbPagesResponses > 1 && 
                                    <Pagination maxPage={this.state.nbPagesResponses} currentPage={this.state.currentPageResponse} pageChange={this.pageChangeResponse.bind(this)}/>
                                }
                            </div>
                        </Tab>
                        {/* Si l'utilisateur connecté à le role admin, affhichage de 
                        l'onglet d'administration du forum */}
                        {this.props.userConnect.roles.includes("ROLE_ADMIN") &&
                            <Tab eventKey="admin" title={`Administration de ${APPNAME}`}>
                                <div className="container-fluid">
                                    <div className="row">
                                        <div className="col-xl-6 col-lg-12 overflow-auto">
                                            <h3>Les posts:</h3>
                                            <AdminPost/>
                                        </div>
                                        <div className="col-xl-3 col-lg-6">
                                            <h3>Les Catégories:</h3>
                                            <AdminCategories reload={this.props.reload}/>
                                        </div>
                                        <div className="col-xl-3 col-lg-6">
                                            <h3>Les Utilisateurs:</h3>
                                            <AdminUsers/>
                                        </div>
                                    </div>
                                </div>
                            </Tab>
                        }
                    </Tabs>
                </Fragment>

                }
            </Fragment>
        );
    }
}
export default UserInfos;