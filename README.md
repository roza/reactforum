![](showcase.png)
<h3 align="center">ReactForum</h3>
<p align="center">
  <p>
    <strong>Projet PHP-REST-JS : Nous avons chosi de mettre en place le projet du forum.</strong>
    <br />
    Ce dernier est décomposé en 2: 
    <br />  
    - L'api qui est développé à l'aide de Symfony et API Platform.
    <br />
    - Le front en Single Page Application qui est développé en ReactJS.
    <br/> 
    - L'authentification à l'api se fait grâce à un token JWT.
    <br />
    <!-- <a href="https://gitlab.com/meunier.jeason/reactforum"><strong>Explore the docs »</strong></a> -->
    <br />
    <br />
    <p align="center">
        <a href="https://forum.boom-ker.live/">View Demo</a>
        ·
        <a href="https://gitlab.com/meunier.jeason/reactforum/issues">Report Bug</a>
        ·
        <a href="https://gitlab.com/meunier.jeason/reactforum/issues">Request Feature</a>
    </p>
  </p>
</p>



<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary><h2 style="display: inline-block">Sommaire</h2></summary>
  <ol>
    <li>
      <a href="#a-propos-du-projet">A propos du projet</a>
      <ul>
        <li><a href="#outils-utilisés">Outils utilisés</a></li>
      </ul>
    </li>
    <li>
      <a href="#démarrage">Démarrage</a>
      <ul>
        <li><a href="#pré-requis">Pré-requis</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#utilisation">Utilisation</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## A propos du projet

<!-- [![Product Name Screen Shot][product-screenshot]](https://example.com) -->
**Attention, ce repo n'est que la partie Front-End du projet**
Ce travail est le résultat du projet PHP-JS-REST de la licence PRO WEB & MOBILE d'Orléans:

Le but est de développer un petit forum en PHP. Ce forum comportera des catégories prédéfinies et les utilisateurs enregistrés pourront y ajouter des sujets ou ajouter des réponses aux sujets existants. 
Les fonctionnalités suivantes sont demandées :
* Un visiteur non authentifié a le droit de voir toutes les pages du forum mais pas de publier
* Un visiteur authentifié à la droit de faire un post dans une catégorie en y créant un nouveau sujet ou en répondant à un sujet existant
* Un administrateur a le droit de créer des utilisateurs et est aussi modérateur de ce forum.
* Implémentez une API JSON permettant par exemple de récupérer tous les posts d’une certaine catégorie et chaque post individuellement.
* Si possible : Réaliser l’application via une SPA (Single Page Application).
* Utilisez un thème responsive


### Outils utilisés

Pour la mise en place de l'API
* [Symfony 5](https://symfony.com/)
* [API Platform](https://api-platform.com/)
* [Lexit JWT Token](https://github.com/lexik/LexikJWTAuthenticationBundle)

Pour la mise en place du Front-End
* [ReactJS](https://fr.reactjs.org/)
* [Create-React-App](https://github.com/facebook/create-react-app)
* [Npm](https://www.npmjs.com/)


<!-- GETTING STARTED -->
## Démarrage

Afin de déployer localement une copie de ce Front-End développé en React, merci de suivre les étapes suivantes.

### Pré-requis
**Attention, il faut d'abord installer l'api disponible ici: [ForumApi](https://gitlab.com/meunier.jeason/forumapi)**
Pour déployer ce Front-End, les prérequis sont les suivants:
* Node.js et Npm
  ```sh
  # Installation de Node.js en version LTS sur Debian10
  sudo apt-get install curl software-properties-common
  curl -sL https://deb.nodesource.com/setup_12.x | sudo bash -
  sudo apt-get install nodejs
  # Vérifier les versions de node et npm:
  node -v # doit retourner la version 12.x
  npm -v # doit retourner la version 6.x 
  ```

### Installation

1. Cloner ce repo
   ```sh
   git clone https://gitlab.com/meunier.jeason/reactforum.git
   ```
2. Installation
   Dans un premier temps, nous allons installer les dépendances:
   ```sh
   cd reactforum
   npm i
   ```
   Ensuite, nous devons modifier l'adresse de l'api dans le fichier src/components/constants.js:
   ```sh
    # src/components/constants.js 
    export const API = 'http://192.168.1.20:8000/api'
    # Dans notre cas, l'api et le forum sont installé sur une VM
    # Nous devons donc mettre l'adresse IP de la machine virtuel
    # Si vous etes en local, vous pouvez mettre localhost
    # Vous pouvez aussi changer le nom du forum
    export const APPNAME = 'Choisissez un nom pour votre forum';
   ```

Le front-end est maitenant prêt à fonctionner!

Pour lancer le serveur:
```sh
npm start
```

## Utilisation
<a href="https://forum.boom-ker.live/">Démo en ligne</a>
Vous pouvez maintenant acceder au forum à l'adresse http://ip-du-front:3000

Le projet est fourni avec 4 utilisateurs:
* Un administrateur:
  * username: admin
  * password: admin
* Trois utilisateurs:
  * username: user1 / user2 / user3
  * password: user1 / user2 / user3

Lorsque vous n'êtes pas connecté:
* La page d'accueil affiche les posts du plus récent au plus ancien.
* Depuis la navbar, vous pouvez vous connecter ou vous enregistrer.
* Depuis la navbar, vous pouvez effectuer une recherche sur les posts (cette recherche prend en compte le titre et le contenu des posts).
* Vous pouvez afficher les posts d'une catégorie en particulier à l'aide de la liste déroulante de la navbar.
* En cliquant sur un post, vous accederez au post voulu et aux réponses de celui cliquant.
* En cliquant sur le nom des utilisateurs qui ont postés une question ou une réponse, vous accederez à tous les posts et toutes les réponses de cet utilisateur.

Lorsque vous êtes connecté:
* Depuis toutes les pages affichant les posts, vous pouvez éditer ou supprimer un post vous appartenant.
* Depuis la page d'un post en particulier, vous pouvez éditer ou supprimer une réponse qui vous appartient.
* Vous pouvez acceder à tous vos posts et vos réponses en cliquant sur votre nom dans la navbar.

Si vous êtes connecté en temps qu'admin:
* Vous pouvez modifier/supprimer tous les posts et toutes les réponses depuis n'importe quel page du forum
* En cliquant sur votre nom dans la navbar, vous avez un onglet d'administration depuis lequel vous pourrez:
  * Supprimer/Modifier les posts du forum
  * Ajouter/Supprimer des catégories
  * Supprimer des utilisateurs (sauf les autres admins!!)
  
## Développeurs/Contributeurs

Jeason Meunier / Damien Riolet

Lien du projet: [https://gitlab.com/meunier.jeason/reactforum](https://gitlab.com/meunier.jeason/reactforum)
