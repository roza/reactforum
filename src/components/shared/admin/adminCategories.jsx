import React, { Component, Fragment } from 'react';
import { API } from '../../constants.js';
import CategoryLine from './categoryLine';
import Table from 'react-bootstrap/Table';
import { RiAddCircleLine } from "react-icons/ri";
import CategoryModal from './categoryModal';

/**
 * Composant affichant le tableau des catégories sur la page Admin
 * Enfant de UserInfos, Parent de CategoryModal, CategoryLine
 * Props requis: reload (fonction parente de rechargement des catégories pour la navbar)
 *
 * @class AdminCategories
 * @extends {Component}
 */
class AdminCategories extends Component {
    
    state = {
        categories: [],
        modalShowToggle: false
    }

    /**
     * Retourne toute les catégories de l'api
     *
     * @memberof AdminCategories
     */
    getAllCategories(){
        const requestOptions = {
            method: 'GET',
            headers: { 'Content-Type': 'application/json' },
            credentials: 'omit'
        };
        fetch(`${API}/categories`, requestOptions)
        .then(res => res.json())
          .then(res => {
            this.setState({ 
                categories: res['hydra:member'],
            })
        }).catch(console.log)
    }

    /**
     * Gere l'etat de la modal de modification
     *
     * @memberof AdminCategories
     */
    modalPopUpHandler=()=>{
        this.setState({
            modalShowToggle: !this.state.modalShowToggle
        })
    }

    /**
     * Au didMount du composant, appel à la fonction qui fetch l'api
     *
     * @memberof AdminCategories
     */
    componentDidMount(){
        this.getAllCategories();
    }

    /**
     * Promesse permettant l'affichage du LoadingSpinner
     *
     * @return {Promise} 
     * @memberof AdminCategories
     */
    loadingTime(){
        return new Promise(resolve => setTimeout(resolve, 100))
    }

    /**
     * En cas de suppression/ajout de catégorie, appel la 
     * fonction parent de reload des catégories
     *
     * @memberof AdminCategories
     */
    updateList(){
        this.loadingTime().then(() => {
            this.props.reload()
        })

    }

    render(){
        return (
            <Fragment >
                <Table className="bg-light" striped bordered hover size="sm">
                    <thead>
                        <tr>
                            <th>Nom</th>
                            <th>Actions
                                
                                <button type="button" className="close" onClick={() => this.modalPopUpHandler()}>
                                    <RiAddCircleLine/>
                                </button>
                                <CategoryModal show={this.state.modalShowToggle} modalUpdate={this.modalPopUpHandler.bind(this)} updateList={this.updateList.bind(this)}></CategoryModal>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.categories.map((category,i , arr) => {
                            return(
                                <CategoryLine key={category.id} category={category} updateList={() => this.updateList()} /> 
                            )
                        })}
                    </tbody>
                </Table>
            </Fragment>
        );
    }
}
export default AdminCategories;